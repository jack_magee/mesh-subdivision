#include <vector>
#include <cmath>
#include <iostream>

#include "io.h"
#include "diredge.h"
#include "Cartesian3.h"
#include "subdivide.h"

using namespace std;
using namespace diredge;


int main(int argc, char **argv)
	{ // main()
	if (argc != 4)
		{ // wrong # of arguments
		std::cout << "Usage: %s infile outfile iterations" << std::endl;
		return 0;
		} // wrong # of arguments
	
    // Set up input parameters
    string inputFilename = argv[1];
    string outputFilename = argv[2];
    int maxIterations = atoi(argv[3]);

	// Read in the triangle soup to a vector for processing
    vector<Cartesian3> soup = io::readTriangleSoup(inputFilename);

	// Create mesh from the triangle soup
    diredge::diredgeMesh mesh = diredge::createMesh(soup);

	
	//iterate for maxiterations
	diredge::diredgeMesh sub_mesh;
	for(int i = 0; i < maxIterations; i++)
	{
		//Create subdivided mesh from directed edge
		sub_mesh = subdivide::subdivideMesh(mesh);
		mesh = sub_mesh;
	}
	
	//Write new mesh to soup so can be outputted
	vector<Cartesian3> sub_soup = diredge::makeSoup(sub_mesh);

	// write the triangle soup back out
	io::writeTriangleSoup(argv[2], sub_soup);

    return 0;
	} // main()
