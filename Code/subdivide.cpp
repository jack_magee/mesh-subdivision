#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>

#include "subdivide.h"
#include "diredge.h"

using namespace std;
using namespace diredge;

#define TAU 6.28219

//the value of alpha we need in the calculation for the loop subdivision
/*Inputs: n - this is the degree of the vertex we are calculating alpha for
  Outputs: returns the value of alpha*/
float calculateAlpha(int n){
    return (0.625f - ((0.375f + 0.25f*cos(TAU/n))*(0.375f + 0.25f*cos(TAU/n))))/n;
}

//find the new positions of the vertices
/*Inputs: mesh - takes in the mesh we are working on and updates the positions of the appropriate vertices
          oldOrNew - a bool vector that says whether a given vertex is an old one to be moved, or new to be left
          faceVerices - the old face definitions so that the 1-ring can be calculated
  Outputs: updates the positions in the mesh*/
void findNewPositions(diredgeMesh &mesh, vector<bool> &oldOrNew, vector<long> faceVertices){
    vector<Cartesian3> oldPositions = mesh.position; //keep a vector of old positions so we arent overriding and reusing moved vertices
    vector<Cartesian3> newPositions = vector<Cartesian3>(oldPositions.size(), Cartesian3()); //empty vector to store new positions
    
    //go through all the old vertices
    for(int k = 0; k <  oldPositions.size(); k ++){
        //and check if it is old or new
        if(oldOrNew[k] == 0){ //the vertex is old and needs to be moved
            long currentVertex = k; //current vertex we are dealing with
            Cartesian3 oneRingSum(0.0f,0.0f,0.0f); //keep track of the sum of the positions of the one ring
            int degree = 0; //degree of the current vertex
            //iterate over all the edges to calculate degree of the current vertex and the 1-ring sum
            for(int i = 0; i < faceVertices.size(); i++){
                if (faceVertices[i] == currentVertex){ //if the point exists then an edge exists
                    degree++;
                    oneRingSum = oneRingSum + oldPositions[faceVertices[NEXT_EDGE(i)]]; //find the position of the vertex at the end of the edge we have found
                }
            }
            float alpha; //used in the loop subdivision formula
            if(degree == 3){ 
                alpha = 3.0f/16.0f;
            }
            else{
                alpha = calculateAlpha(degree); //calculate the value of alpha for a given degree
            }
            newPositions[k] = oldPositions[k]*(1.0f - degree*alpha) + oneRingSum * alpha; //calculate the new positon of a vertex with the loop subdivision formula
        }
        else{ //the vertex is new and can be left as it is
           newPositions[k] = oldPositions[k]; 
        }
    }
    mesh.position = newPositions; //update the mesh with the new positions
}

//find and return the position of the vertex inbetween vertices
/*Inputs: v_0, v_1 - are the vertices at the end of the edge
          v_2, v_3 - are the vertices either side of the edge
  Outputs: v_01 - the position of the vertex w.r.t edge v_0 and v_1*/
Cartesian3 getMidpoint(Cartesian3 v_0, Cartesian3 v_1, Cartesian3 v_2, Cartesian3 v_3){
    Cartesian3 v_01;
    v_01 = (v_0 + v_1)*0.375f + (v_2 + v_3)*0.125f; //use forula to find the positon of the new vertex
    return v_01;
}

//function to find out if a postion is in the array of vertices already, if so return the indices, else add to the end.
/*Inputs: pointToCheck - the vertex we are checking to see if it exists already in ..
          positions - the vector of positions, we dont want to duplicate!
          oldOrNew - bool vector to state whether a vertex is old or new
  Outputs: posInVector - the indices of where the point is*/
long ifElement(Cartesian3 pointToCheck, vector<Cartesian3> &positions, vector<bool> &oldOrNew){
    long posInVector; //the index where the point is in the vector
    //Iterate through the vector and check to see if the point exists
    for(int i = 0; i < positions.size(); i++){
        if (pointToCheck == positions[i]){
            posInVector = i; //if it does then i is the index of it
            return posInVector;
        }
    }
    //if it doeesnt exist, then add the point to the end of the vector
    positions.push_back(pointToCheck);
    oldOrNew.push_back(1); //state that the point is new
    posInVector = positions.size()-1; //and the index is the end of the vector
    return posInVector;
}

//Function to subdivide a mesh once, first it splits each face into 4, then it repositions the vertices
/*Inputs: old_mesh - this is the mesh we are going to be subdividing
  Outputs: new_mesh - this is the subdivided mesh*/
diredgeMesh subdivide::subdivideMesh(diredgeMesh old_mesh){
    //we only really care that the vertexes are in the correct order and the faces are correct
    diredgeMesh new_mesh;
    new_mesh.faceVertices = old_mesh.faceVertices;
    new_mesh.position = old_mesh.position;
    vector<bool> oldOrNew = vector<bool>(old_mesh.position.size(),0);

    //iterate over each of the faces and extract the corresponding vertices
    for(int i = 0; i < old_mesh.faceVertices.size()/3; i++){
        //get the vertices of the triangle
        Cartesian3 v_0 = old_mesh.position[old_mesh.faceVertices[i*3 + 0]];
        Cartesian3 v_1 = old_mesh.position[old_mesh.faceVertices[i*3 + 1]];
        Cartesian3 v_2 = old_mesh.position[old_mesh.faceVertices[i*3 + 2]];

        //get the opposite vertex for each of the midpoints
        Cartesian3 v_01_opp = old_mesh.position[old_mesh.faceVertices[PREVIOUS_EDGE(old_mesh.otherHalf[i*3 + 0])]];
        Cartesian3 v_12_opp = old_mesh.position[old_mesh.faceVertices[PREVIOUS_EDGE(old_mesh.otherHalf[i*3 + 1])]];
        Cartesian3 v_20_opp = old_mesh.position[old_mesh.faceVertices[PREVIOUS_EDGE(old_mesh.otherHalf[i*3 + 2])]];

        //Calculate the midpoints between all the vertices
        Cartesian3 v_01 = getMidpoint(v_0, v_1, v_2, v_01_opp); //midpoint between v_0 and v_1
        Cartesian3 v_12 = getMidpoint(v_1, v_2, v_0, v_12_opp); //midpoint between v_1 and v_2
        Cartesian3 v_20 = getMidpoint(v_2, v_0, v_1, v_20_opp); //midpoint between v_2 and v_0

        //Put the center triangle in the face vector
        new_mesh.faceVertices[i*3 + 0] = ifElement(v_01, new_mesh.position, oldOrNew);
        new_mesh.faceVertices[i*3 + 1] = ifElement(v_12, new_mesh.position, oldOrNew);
        new_mesh.faceVertices[i*3 + 2] = ifElement(v_20, new_mesh.position, oldOrNew);

        //Put the surrounding triangles in the face vector
        //face 0
        new_mesh.faceVertices.push_back(ifElement(v_0, new_mesh.position, oldOrNew));
        new_mesh.faceVertices.push_back(ifElement(v_01, new_mesh.position, oldOrNew));
        new_mesh.faceVertices.push_back(ifElement(v_20, new_mesh.position, oldOrNew));

        //face 1
        new_mesh.faceVertices.push_back(ifElement(v_1, new_mesh.position, oldOrNew));
        new_mesh.faceVertices.push_back(ifElement(v_12, new_mesh.position, oldOrNew));
        new_mesh.faceVertices.push_back(ifElement(v_01, new_mesh.position, oldOrNew));

        //face 2
        new_mesh.faceVertices.push_back(ifElement(v_2, new_mesh.position, oldOrNew));
        new_mesh.faceVertices.push_back(ifElement(v_20, new_mesh.position, oldOrNew));
        new_mesh.faceVertices.push_back(ifElement(v_12, new_mesh.position, oldOrNew));

    }
    //details for filling in the mesh structure information
    new_mesh.otherHalf = vector<long>(new_mesh.faceVertices.size(), -1);
    new_mesh.normal = vector<Cartesian3>(new_mesh.position.size(), Cartesian3());
    new_mesh.firstDirectedEdge = vector<long>(new_mesh.position.size(), -1);

    //update the positions of the old vertices
    findNewPositions(new_mesh, oldOrNew, old_mesh.faceVertices);
    diredge::makeDirectedEdges(new_mesh);
    
    return new_mesh; //return the updated mesh
}