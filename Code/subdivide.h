#pragma once

#include <string>
#include <vector>
#include "Cartesian3.h"
#include "diredge.h"

namespace subdivide
{
//Takes a half edge data structure and subdivides it, outputting the subdivided mesh
diredge::diredgeMesh subdivideMesh(diredge::diredgeMesh);
}