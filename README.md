Mesh Subdivision CW1

This takes in a .tri file and subdivides the mesh a specified number of times and returns the subdivided .tri file.
 
To run:
1) Open up a terminal in the 'Code' folder.
2) Run 'make'
3) Type - './subdivision ../Models/*IN_FILE_NAME*.tri ../Output\ Models/*OUT_FILE_NAME*.tri *n*'
This will output a .tri file into the output models folder, that has been subdivided n times, n here is an integer.

Example: './subdivision ../Models/octahedron.tri ../Output\ Models/octatest.tri 5'
